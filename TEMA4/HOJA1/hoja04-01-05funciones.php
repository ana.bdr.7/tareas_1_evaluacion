<?php

define("HOST", "127.0.0.1");
define("DB", "dwes_01_nba");
define("USER", "root");
define("PASS", "");

function conexion_pdo(){ 

   $port = "3306";
   $charset = 'utf8mb4';
   
   $options = [
       \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
       \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
       \PDO::ATTR_EMULATE_PREPARES   => false,
   ];

   $dsn = "mysql:host=" .HOST.";dbname=".DB.";charset=$charset;port=$port";

   try {
        $pdo = new \PDO($dsn, USER, PASS, $options);
        //echo 'Conexion correcta';
   } catch (\PDOException $e) {
        //echo 'Error';
        throw new \PDOException($e->getMessage(), (int)$e->getCode());
   }

   return $pdo;


}

function conexion_mysqli(){

    $conn = new mysqli(HOST, USER, PASS, DB);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    return $conn;
}

function getEquipos(){

    $pdo = conexion_pdo();

    $equipos = $pdo->query("select * from equipos")->fetchAll();
    
    return $equipos;

}

function getJugadores($equipo){
    $pdo = conexion_pdo();

    $jugadores = $pdo->query("SELECT * FROM `jugadores` where nombre_equipo = '" . $equipo . "'")->fetchAll();

    return $jugadores;
}

function getPosicion(){
    $pdo = conexion_pdo();

    $posicion = $pdo->query("select DISTINCT posicion from jugadores")->fetchAll();

    return $posicion;
}

function cambioJugador($equipo_elegido,$jugador_elegido,$nombre_nuevo,$procedencia,$altura,$peso,$posicion){

    $pdo = conexion_pdo();
 

    $success = true;

    try{
        
        $pdo->beginTransaction();

        $preparedStatement = $pdo->exec("DELETE from estadisticas where jugador = (select codigo from jugadores where nombre = '$jugador_elegido')");

        $preparedStatement = $pdo->exec("DELETE from jugadores where nombre = '$jugador_elegido'");

        $pdo->exec("INSERT INTO jugadores(nombre,procedencia,altura,peso,posicion,nombre_equipo) VALUES ('".$nombre_nuevo."', '".$procedencia."', '".$altura."', '".$peso."', '".$posicion."' ,'".$equipo_elegido."')");
    
        $pdo->commit();

    }catch(Exception $e) {
        
        if($pdo->inTransaction()){
            $pdo->rollback();
        }
        $success = false;
    }
    $pdo = null;
    return $success;
}

function modificar_peso($array){

    $pdo = conexion_pdo();
    $success = true;
    try{
        
        $pdo->beginTransaction();

        foreach($array as $nombre => $peso){  
            
            $peso_nuevo = floatval($peso);
            $nombre_nuevo = (string)$nombre;
            echo $peso_nuevo ." / " . $nombre_nuevo;
            $nuevo_peso = 100;
            $nuevo_nombre = "Calvin Booth";
            
            /*$query = "UPDATE jugadores SET peso=? WHERE nombre=?";
            $stmt = $pdo->prepare($query);
            $stmt->execute([$peso_nuevo,$nombre]);  */
                 
           $pdo->exec("UPDATE jugadores set peso = '" .$peso_nuevo ."' where nombre = '" .$nombre_nuevo ."'");
            
        }

        $pdo->commit();

    }catch(Exception $e) {
        echo $e;
        if($pdo->inTransaction()){
            $pdo->rollback();
        }
        $success = false;
    }
    $pdo = null;
    return $success;

}
?>