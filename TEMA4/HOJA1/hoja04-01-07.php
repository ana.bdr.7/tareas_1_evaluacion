<?php
    require_once('hoja04-01-05funciones.php');    
?>
<!DOCTYPE html>
<html>
    <head>
        <title>hoja 1 ejercicio 7</title>
        <meta charset="UTF-8">
        <style>
            table{
                padding:5px;
                
            }
            td{
                border: 1px solid black;
                width: 30%;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <h1>Traspasos de jugadores</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']?>" method="post">
            <p>Equipo:
            <?php
                    echo "<select name='equipo_elegido'>";
                    foreach(getEquipos() as $equipo){

                        echo "<option value='" .$equipo['nombre'] ."'>" .$equipo['nombre'] ."</option>";
                        
                    }
                    echo "</select>"
            ?>
            </p>
            <input type="submit" value="Mostrar" name="enviar">
        </form>
        <?php
            if(isset($_POST['enviar'])){
                if(isset($_POST['equipo_elegido']) && !empty($_POST['equipo_elegido'])){
                    $equipo_elegido = $_POST['equipo_elegido'];

                    echo "<h3>Baja y alta de jugadores: </h3>";
                    echo "<form action='hoja04-01-07.php' method='post'>";
                    echo "<p>Baja de jugador: ";
                    echo "<select name='jugador_elegido'>";
                    foreach(getJugadores($equipo_elegido) as $jugadores){
                        echo "<option value='" .$jugadores['nombre'] ."'>" .$jugadores['nombre'] ."</option>";
                    }
                    echo "</select></p>";
                    echo "<h3>Datos del nuevo jugador:</h3>";
                    echo "<p>Nombre: <input type='text' name='nombre'></p>";
                    echo "<p>Procedencia: <input type='text' name='procedencia'></p>";
                    echo "<p>Altura: <input type='number' placeholder = '0.00' step='0.01' name='altura'></p>";
                    echo "<p>Peso: <input type='number' placeholder = '0.00' step='0.01' name='peso'></p>";
                    echo "<p>Posicion: <select name='posicion_elegida'>";
                    foreach(getPosicion() as $posicion){
                        echo "<option value='" .$posicion['posicion'] ."'>" .$posicion['posicion'] ."</option>";
                    }
                    echo "</select></p>";
                    echo "<input type='hidden' name='equipo_elegido' value= $equipo_elegido>";
                    echo "<input type='submit' value='Realizar Traspaso' name='traspaso'>";
                    echo "</form>";

                }
                
            }

            if(isset($_POST['traspaso'])){
                   
                

                if(
                    
                    isset($_POST['jugador_elegido']) && !empty($_POST['jugador_elegido'])&&
                    isset($_POST['nombre']) && !empty($_POST['nombre']) &&
                    isset($_POST['procedencia']) && !empty($_POST['procedencia']) &&
                    isset($_POST['altura']) && !empty($_POST['altura']) &&
                    isset($_POST['peso']) && !empty($_POST['nombre']) &&
                    isset($_POST['posicion_elegida']) && !empty($_POST['nombre'])
                ){
                    
                    $equipo_elegido = $_POST['equipo_elegido'];
                    $jugador_elegido = $_POST['jugador_elegido'];
                    $nombre = $_POST['nombre'];
                    $procedencia = $_POST['procedencia'];
                    $altura = $_POST['altura'];
                    $peso = $_POST['peso'];
                    $posicion_elegida = $_POST['posicion_elegida'];
                   

                    if(cambioJugador($equipo_elegido,$jugador_elegido,$nombre,$procedencia,$altura,$peso,$posicion_elegida) == true){
                        echo "cambio realizado";
                    }else{
                        echo "ha ocurrido un error";
                    };
                }else{
                    echo "error";
                }
            }

            

          
        ?>
    </body>
</html>