<?php
    require_once('hoja04-01-05funciones.php');    
?>
<!DOCTYPE html>
<html>
    <head>
        <title>hoja 1 ejercicio 5  </title>
        <meta charset="UTF-8">
        <style>
            table{
                padding:5px;
                
            }
            td{
                border: 1px solid black;
                width: 30%;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <h1>Jugadores de la NBA</h1>
        <form action="<?php echo $_SERVER['PHP_SELF']?>" method="post">
            <p>Equipo
                <?php
                    echo "<select name='equipo_elegido'>";
                    foreach(getEquipos() as $equipo){
                        echo "<option value='" .$equipo['nombre'] ."'>" .$equipo['nombre'] ."</option>";
                        
                    }
                    echo "</select>"
                ?>

            </p>
            <input type="submit" value="Buscar" name="enviar">
        </form>

        <?php
            if(isset($_POST['enviar'])){
                if(isset($_POST['equipo_elegido']) && !empty($_POST['equipo_elegido'])){
                    $equipo_elegido = $_POST['equipo_elegido'];

                    echo "<table>";

                    foreach(getJugadores($equipo_elegido) as $key => $jugadores){
                        echo "<tr><td>". $jugadores['nombre'] ."</td></tr>";
                        
                    }

                    echo "</table>";
                }
            }
        ?>
    
    </body>
</html>