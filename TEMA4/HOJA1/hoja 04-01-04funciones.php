<?php

define("HOST", "127.0.0.1");
define("DB", "dwes_01_nba");
define("USER", "root");
define("PASS", "");

function conexion_pdo(){ 

   $port = "3306";
   $charset = 'utf8mb4';
   
   $options = [
       \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
       \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
       \PDO::ATTR_EMULATE_PREPARES   => false,
   ];

   $dsn = "mysql:host=" .HOST.";dbname=".DB.";charset=$charset;port=$port";

   try {
        $pdo = new \PDO($dsn, USER, PASS, $options);
        //echo 'Conexion correcta';
   } catch (\PDOException $e) {
        //echo 'Error';
        throw new \PDOException($e->getMessage(), (int)$e->getCode());
   }

   return $pdo;


}

function conexion_mysqli(){

    $conn = new mysqli(HOST, USER, PASS, DB);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    return $conn;
}

function getEquipos(){

    $pdo = conexion_pdo();

    $equipos = $pdo->query("select * from equipos")->fetchAll();
    
    return $equipos;

}


?>
