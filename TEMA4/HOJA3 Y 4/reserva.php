<?php

    require('gestionBBDD.php'); 
    
?>

<!DOCTYPE html>
<html>
    <head>
        <title>Reserva de plaza</title> 
        <meta charset="UTF-8">  
    </head>
    <body>
        <h1>Reserva de asiento</h1>
        <form action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
            <p>Nombre
                <input type="text" name="nombre" placeholder="Su nombre">
            </p>
            <p>DNI
                <input type="text" name="dni" placeholder="Su DNI">
            </p>
            <p>Asiento
                <?php
                    echo "<select name='asiento_reservado'>";
                    foreach(mostrarPrecios() as $rows => $row){
                                                
                         echo "<option value='" .$row['numero'] ."'>" .$row['numero'] ."(" .$row['precio'] .")" ."</option>";

                    };

                    echo "</select>";

                ?>
            </p>

            <input type="submit" name="enviar" value="Buscar">

        </form>

        <?php
            if(isset($_POST['enviar'])){

                if(isset($_POST['nombre']) && !empty($_POST['nombre']) &&
                    isset($_POST['dni']) && !empty($_POST['dni']) &&
                    isset($_POST['asiento_reservado']) && !empty($_POST['asiento_reservado'])){

                        $nombre = $_POST['nombre'];
                        $dni = $_POST['dni'];
                        $asiento = $_POST['asiento_reservado'];

                    if(guardar_mysqli($nombre,$dni,$asiento)){
                        echo "Se ha reservado el asiento " .$_POST['asiento_reservado'];
                    }else{
                        echo "No se ha podido realizar la reserva";
                    }
                        
                }

            }
        ?>
    </body>
</html>