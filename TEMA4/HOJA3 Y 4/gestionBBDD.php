<?php
    define("HOST", "127.0.0.1");
    define("DB", "dwes_03_funicular");
    define("USER", "root");
    define("PASS", "");

    function conexion_pdo(){ 

        $port = "3306";
        $charset = 'utf8mb4';
        
        $options = [
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES   => false,
        ];
     
        $dsn = "mysql:host=" .HOST.";dbname=".DB.";charset=$charset;port=$port";
     
        try {
             $pdo = new \PDO($dsn, USER, PASS, $options);
             //echo 'Conexion correcta';
        } catch (\PDOException $e) {
             //echo 'Error';
             throw new \PDOException($e->getMessage(), (int)$e->getCode());
        }
     
        return $pdo;
     
     
     }
    
    function conexion_mysqli(){

        $conn = new mysqli(HOST,USER,PASS,DB);
        if($conn->connect_error){
            die("Connection failed: " . $conn->connect_error);
        }
        return $conn;
    }

    function mostrarPrecios(){

        $pdo = conexion_pdo();
        
        $precio = $pdo->query("select numero,precio from plazas where reservada = 0")->fetchAll();       
           
        return $precio;
    }


    //con pdo
    function llegada(){

        $pdo = conexion_pdo();

        $success = true;

        try { 
            //inicia la transaccion 
            $pdo->beginTransaction();         
            
            //querys
            $pdo->exec("update plazas set reservada = 0");
                    
            $pdo->exec("delete from pasajeros");
            
            // $pdo->rollback(); esto es si algo ha ido mal no ejecuta nada

            //cierra transaccion y la ejecuta aunque alguna no funciona
            $pdo->commit();

        } catch(Exception $e) {
            if($pdo->inTransaction()){
                $pdo->rollback();
            }
            $sucess = false;
        }
        
        //cerrar la conexion con pdo
        $pdo=null;  
        
        return $success;

    }

    //con mysqli

    function llegada_mysqli(){

        $mysqli = conexion_mysqli();
        $success = true;

        try{

            $mysqli->begin_transaction();

            $mysqli->query("update plazas set reservada = 0");
                    
            $mysqli->query("delete from pasajeros");

            $mysqli->commit();


        }catch(Exception $e) {
            $mysqli->rollback();
            $success=false;
        }

        $mysqli->close();

        return $success;

    }

    function guardar_pdo($nombre,$dni,$numero){
        $pdo = conexion_pdo();

        $success = true;

        try{
            //inica la transaccion
            $pdo->beginTransaction();

            // Insert the metadata of the order into the database
            $preparedStatement = $pdo->prepare(
                'INSERT INTO `pasajeros` (`dni`, `nombre`, `numero_plaza`, `sexo`)
                VALUES (:dni, :nombre, :numero, :sexo)'
            );
            
            $preparedStatement->execute([
                'dni' => $dni,
                'nombre' => $nombre,
                'numero' => $numero,
                'sexo' => "-"
            ]);

            $preparedStatement = $pdo->prepare(
                'update `plazas` set `reservada` = 1 where numero= :numero'
            );
            
            $preparedStatement->execute([
                'numero' => $numero                
            ]);

            $pdo->commit();

        } catch(Exception $e) {

            if($pdo->inTransaction()){
                $pdo->rollback();
            }
            $sucess = false;
        }
        
        //cerrar la conexion con pdo
        $pdo=null;  
        
        return $success;
    }

    function guardar_mysqli($nombre,$dni,$numero){
        
        $mysqli = conexion_mysqli();
        $success = true;
        $sexo = "-";

        try{

            $mysqli->query("BEGIN");

            $sql = "INSERT INTO pasajeros (dni, nombre, numero_plaza, sexo) VALUES (?,?,?,?)";
            $stmt = $mysqli->prepare($sql);
            $stmt->bind_param("ssis",$dni,$nombre,$numero,$sexo);
            $stmt->execute();

            $sql = "update `plazas` set `reservada` = 1 where numero= ?";
            $stmt = $mysqli->prepare($sql);
            $stmt->bind_param("i",$numero);
            $stmt->execute();

            $mysqli->query("COMMIT");
            

        } catch(Exception $e) {

            if($mysqli->inTransaction()){
                $mysqli->rollback();
            }
            $sucess = false;
        }
        
        //cerrar la conexion con my$mysqli
        $mysqli->close();  
        
        return $success;

    }

    function mostrar_todos_los_precios(){

        $pdo = conexion_pdo();
        
        $precio = $pdo->query("select numero,precio from plazas")->fetchAll();       
           
        return $precio;

    }

    function actualizar_precios($data){

        $pdo = conexion_pdo();

        $success = true;

        try{            
            //inica la transaccion
            $pdo->beginTransaction();

            foreach($data as $key => $value){

                $preparedStatement = $pdo->prepare(
                    'update `plazas` set `precio` = :value where numero= :key'
                );
                
                $preparedStatement->execute([
                    'value' => $value, 
                    'key' => $key             
                ]);

            }

            $pdo->commit();

        } catch(Exception $e) {

            if($pdo->inTransaction()){
                $pdo->rollback();
            }
            $sucess = false;
        }
        
        //cerrar la conexion con pdo
        $pdo=null;  
        
        return $data;

    }

?>