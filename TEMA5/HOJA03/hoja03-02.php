<?php
    
    session_start();
    
    $_SESSION = time();
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Sesiones</title>
        <meta charset="UTF-8">
        
    </head>
    <body>
    <?php
        $fecha = date_create($_SESSION);
        echo "Has entrado a la " .date_format($fecha, "d/m/Y H:i:s");
    ?>
    </body>
</html>