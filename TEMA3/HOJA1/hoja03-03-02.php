<!DOCTYPE html>
    <head>
        <title>Hoja03-03-02</title>
        <meta charset="UTF-8"></meta>
    </head>
    <body>
        <?php
        $cantidad = 583;
        
        $billetes = [500,200,100,50,20,10,5,2,1];

        foreach($billetes as $valor){ 
            $resultado = $cantidad/$valor;
            $resultado = (int)$resultado;
            echo "Billetes de $valor: $resultado<br>";
            $cantidad = $cantidad % $valor;
        }

        ?>
    </body>
</html>