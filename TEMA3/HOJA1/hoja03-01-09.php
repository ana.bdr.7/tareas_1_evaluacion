<!DOCTYPE html>
    <head>
        <title>Hoja03-01-09</title>
        <meta charset="UTF-8"></meta>
    </head>
    <body>
        <?php
        $distancia=200;
        $dias=8;
        $precio=2.5;
        $precioTotal = 0;

        if($dias<7){
            $precioTotal = ($distancia*$precio);
            
        }else if($dias>=7){
            $precioTotal = ($distancia*$precio)*0.3;
        }

        echo "Su viaje de $distancia durante $dias días, tiene un precio de $precioTotal";
        ?>
    </body>
</html>