<!DOCTYPE html>
    <head>
        <title>Hoja03-01-01</title>
        <meta charset="UTF-8"></meta>
    </head>
    <body>
        <?php
            $diasSemana = ['','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];
            $mes = [''.'Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
            $anno = date('Y');
            $dia = date('d');
            $dia_mes = date('n');
            $dia_semana = date('w');
            $date = $diasSemana[$dia_semana].", $dia de ".$mes[$dia_mes] ." de $anno";
            echo $date;
        ?>
    </body>
</html>