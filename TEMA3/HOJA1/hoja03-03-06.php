<!DOCTYPE html>
    <head>
        <title>Hoja03-03-06</title>
        <meta charset="UTF-8"></meta>
    </head>
    <body>
        <?php
        $verbos = ['cantar','repartir','aprender'];
        $terminacionesEnAr = ['o','as','a','mos','ais','an'];
        $terminacionesEnEr = ['o','es','e','emos','eis','en'];
        $terminacionesEnIr = ['o','es','e','imos','is','en'];

        foreach($verbos as $verbo){

            $resto = substr($verbo,-2);
            if($resto == "ar"){
                $conjugar = substr($verbo,0,-2);
                
                foreach($terminacionesEnAr as $terminacionEnAr){ 
                    
                    $conjugado = $conjugar . $terminacionEnAr;
                    echo $conjugado . "<br>";
                }
            }elseif($resto == "er"){
                $conjugar = substr($verbo,0,-2);
                
                foreach($terminacionesEnEr as $terminacionEnEr){ 
                    
                    $conjugado = $conjugar . $terminacionEnEr;
                    echo $conjugado . "<br>";
                }
            }else{
                $conjugar = substr($verbo,0,-2);
                
                foreach($terminacionesEnIr as $terminacionEnIr){ 
                    
                    $conjugado = $conjugar . $terminacionEnIr;
                    echo $conjugado . "<br>";
                }
            }
        }
        ?>
    </body>
</html>