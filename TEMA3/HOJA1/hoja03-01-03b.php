<!DOCTYPE html>
    <head>
        <title>Hoja03-01-03</title>
        <meta charset="UTF-8"></meta>
    </head>
    <body>
        <?php
            $numero = 8963;
            $inverso = 0;
            $aux = $numero;

            while($aux !=0){
                //da la vuelta al numero
                $resto = $aux%10;
                
                $inverso = $inverso *10 + $resto;
                
                $aux=(int)($aux/10);
            }

            if($numero == $inverso){
                echo "El $numero es capicúa";
            }else{
                echo "El $numero no es capicúa";
            }
        ?>
    </body>
</html>