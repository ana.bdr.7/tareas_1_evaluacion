<!DOCTYPE html>
    <head>
        <title>Hoja02-02-07</title>
        <meta charset="UTF-8"></meta>
    </head>
    <body>
        <?php
            $variable1 = 25;
            $variable2 = 36;

            echo "valor variable 1 sin cambiar $variable1<br>";
            echo "valor variable 2 sin cambiar $variable2<br>";

            $variablea = $variable1;
            $variable1=$variable2;
            $variable2=$variablea;

            echo "valor variable 1 cambiada $variable1<br>";
            echo "valor variable 2 cambiada $variable2<br>";
        ?>
    </body>
</html>