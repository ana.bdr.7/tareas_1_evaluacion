<!DOCTYPE html>
    <head>
        <title>Hoja02-02-04</title>
        <meta charset="UTF-8"></meta>
    </head>
    <body>
        <?php
            $nombre = "Juan";

            echo "Información de la variable 'nombre':"; var_dump($nombre);
            echo "<br>Contenido de la variable: $nombre";
            $nombre = "NULL";
            echo "<br>Después de asignarle un valor nulo: $nombre";
        ?>
    </body>
</html>