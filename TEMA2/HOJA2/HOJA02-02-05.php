<!DOCTYPE html>
    <head>
        <title>Hoja02-02-05</title>
        <meta charset="UTF-8"></meta>
    </head>
    <body>
        <?php
            $temporal = "Juan";
            echo gettype($temporal);
            echo "<br>";
            $temporal = 3.14;
            echo gettype($temporal);
            echo "<br>";
            $temporal = false;
            echo gettype($temporal);
            echo "<br>";
            $temporal = 3;
            echo gettype($temporal);
            echo "<br>";
            $temporal = null;
            echo gettype($temporal);
            echo "<br>";
        ?>
    </body>
</html>