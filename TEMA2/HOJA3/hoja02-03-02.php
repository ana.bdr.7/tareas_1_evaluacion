<!DOCTYPE html>
    <head>
        <title>Hoja02-03-02</title>
        <meta charset="UTF-8"></meta>
    </head>
    <body>
        <?php
            function restarDias($numdias,$date){

                if(isset($date)){
                    $date = time();
                }
                
                list($hora, $min, $seg, $dia, $mes, $anno) = explode(" ",date("H i s d m Y"));
                
                $d = $dia - $numdias;
                $fecha = date("d-m-Y",mktime($hora,$min,$seg,$mes,$d,$anno));

                return $fecha;
            }

            echo restarDias(8);
        ?>
    </body>
</html>